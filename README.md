# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://mahirfri@bitbucket.org/mahirfri/stroboskop.git

```

Naloga 6.2.3:
https://bitbucket.org/mahirfri/stroboskop/commits/ec2d034bdad6c73e52d1a38c13137bc821afca5b?at=master

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/mahirfri/stroboskop/commits/e9bb19a9a9c453edbc21c98d8964d295eead2f1e?at=izgled

Naloga 6.3.2:
https://bitbucket.org/mahirfri/stroboskop/commits/e38516ff16774c5da9ccaf0ac98c97e43ffded8c?at=izgled

Naloga 6.3.3:
https://bitbucket.org/mahirfri/stroboskop/commits/a4f586735db743a252f5f7d0d1e19b3704bb6ced?at=izgled

Naloga 6.3.4:
https://bitbucket.org/mahirfri/stroboskop/commits/eb821e06c0703b7d7fed4ad4a248eea53ca40f9f?at=izgled

Naloga 6.3.5:

```
git checkout master
git merge --no-ff izgled
git push --all

```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/mahirfri/stroboskop/commits/54be533b7d96b3355fde8f579f5e04da98bf78bb?at=dinamika

Naloga 6.4.2:
https://bitbucket.org/mahirfri/stroboskop/commits/e00fee6b6c4eb5633ea662efbe15117435be0c23?at=dinamika

Naloga 6.4.3:
https://bitbucket.org/mahirfri/stroboskop/commits/2364fdcc35f617353f839597f3670015c69cc82d?at=dinamika

Naloga 6.4.4:
https://bitbucket.org/mahirfri/stroboskop/commits/8363680412ec718371bbfff59cee83f9beaa35bf?at=dinamika